import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { Storage } from '@ionic/storage';

@Component({
	selector: 'page-about',
	templateUrl: 'about.html',
})
export class AboutPage {

	private isAnyCardAvailable: boolean = false;
	private isLoginView: boolean = false;
	private idCardData: any = [{ skeleton: true }];
	private isRetry: boolean = false;
	private loading: boolean = false;
	private optInOfflineIdCard: boolean = true;
	private lastUpdateTime: number = 0;

	constructor(public alertCtrl: AlertController, public navCtrl: NavController, 
		public navParams: NavParams, public sanitized: DomSanitizer) {

		if (navParams.data.idCardInformation) {
			this.isLoginView = true;
			let idCardInformation = navParams.data.idCardInformation;
			this.isAnyCardAvailable = idCardInformation.isAnyCardAvailable;
			this.idCardData = idCardInformation.cards;
		}

	}

	ionViewDidLoad() {

		if (!this.isLoginView) {

			this.fetchIdCardInformation();
		}

	}

	public fetchIdCardInformation() {
		this.loading = true;
		this.isRetry = false;
		this.isAnyCardAvailable = false;

        setTimeout(() => {
            this.loading = false;
        }, 100);
	
	}

	public onChangeViewCardOffline() {

	}


}
