import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
	name: 'capitalize',
})
export class CapitalizePipe implements PipeTransform {

	transform(value: string, ...args) {
		if (value && value.split) {
			let words = value.split(' ');
			for (let i=0, il=words.length; i<il; i++) {
				if (words[i].length > 0) {
					let word = words[0].toLowerCase();
					if (word == 'id') {
						words[i] = words[i].toUpperCase();
					} else {
						words[i] = words[i].charAt(0).toUpperCase() + words[i].slice(1).toLowerCase();
					}
				}
			}
			return words.join(' ');
		}
		return value;
	}
}
