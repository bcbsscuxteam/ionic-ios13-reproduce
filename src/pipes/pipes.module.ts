import { NgModule } from '@angular/core';
import { CapitalizePipe } from './capitalize/capitalize';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [
        CapitalizePipe
    ],
	imports: [
        IonicModule.forRoot(CapitalizePipe)
    ],
	exports: [
        CapitalizePipe
    ]
})
export class PipesModule {}
