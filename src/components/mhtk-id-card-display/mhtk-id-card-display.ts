import { Component, Input, OnChanges } from '@angular/core';
import { Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { TitleCasePipe } from '@angular/common';

declare var Passbook: any;
declare var PhotoViewer: any;

@Component({
    selector: 'mhtk-id-card-display',
    templateUrl: 'mhtk-id-card-display.html'
})
export class MhtkIdCardDisplayComponent implements OnChanges {

    @Input('data') data: any = { skeleton: true };
    @Input('isLoginView') isLoginView: boolean = false;

    coverageTitle: string = "Health";
    get flipTitle(): string {
        return (this.isFlipped) ? "view front of card" : "view back of card";
    };
    isCardAvailable: boolean = true;
    isFlipped: boolean = false;

    imageSrc: any = "assets/images/idcard-skeleton.png";
    imageSrcFront: any = "assets/images/idcard-skeleton.png";
    imageSrcBack: any = "assets/images/idcard-skeleton.png";
    loadingAppleWallet: boolean = false;
    skeleton: boolean = true;
    text: string;
    walletEnabled: boolean;

    constructor(public alertCtrl: AlertController, public sanitizer: DomSanitizer,
        public platform: Platform, public titleCasePipe: TitleCasePipe) {
    }

    ngOnChanges(changes) {
        if (changes.data) {
            var data = changes.data.currentValue;
            this.isCardAvailable = data.isCardAvailable;

            if (data.productCode === "MED") {
                this.coverageTitle = "Health";
            } else if (data.productCode === "DEN") {
                this.coverageTitle = "Dental";
            }

            if (this.isCardAvailable) {

                this.skeleton = false;

                this.imageSrc = this.sanitizer.bypassSecurityTrustUrl(data.base64Image);

                var frontCanvas = document.createElement('canvas');
                frontCanvas.width = 1033 - 20;
                frontCanvas.height = (1310 / 2) - 15;
                var frontCtx = frontCanvas.getContext('2d');

                var backCanvas = document.createElement('canvas');
                backCanvas.width = 1033 - 20;
                backCanvas.height = (1310 / 2) - 15;
                var backCtx = backCanvas.getContext('2d');

                var image = new Image();
                image.onload = () => {
                    frontCtx.drawImage(image, -10, -10);
                    backCtx.drawImage(image, -10, (-1310 / 2) - 5);
                    this.imageSrcFront = frontCanvas.toDataURL("image/png");
                    this.imageSrcBack = backCanvas.toDataURL("image/png");
                };
                image.src = data.base64Image;

            }
            else {
                this.skeleton = !!data.skeleton;
            }
        }

        if (changes.isLoginView) {
            // Disable apple wallet if non-secure (login) view
            this.walletEnabled = this.platform.is('ios') && !changes.isLoginView.currentValue;
        }
    }

    public onAddToAppleWallet() {



        // TODO - Analytics
    }

    public onFlip() {
        if (!this.skeleton) {
            this.isFlipped = !this.isFlipped;
        }
    }

    public onShare() {
        if (!this.skeleton) {
            // STUBBED
        }
    }

    public onTouchMoveCard(e) {
        //e.preventDefault();
        e.stopPropagation();
    }

    public onViewFullscreen() {
        if (!this.skeleton) {
            // STUBBED
        }
    }

    private stringifyServiceCenterAddress(serviceCenter) {
        let addressString = '';
        if (serviceCenter.addresses && serviceCenter.addresses.length > 0) {
            addressString = serviceCenter.addresses[0].lines.join(' \\n');
        }

        if (serviceCenter.phonenumbers && serviceCenter.phonenumbers.length > 0) {
            addressString += '\\n' + serviceCenter.phonenumbers[0].phonenumber;
        }
        return addressString;
    }

    public onAddToWallet() {
        this.loadingAppleWallet = true;

        setTimeout(() => {
            this.loadingAppleWallet = false;
        }, 100);

    }
}
