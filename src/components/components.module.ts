import { NgModule } from '@angular/core';
import { MhtkIdCardDisplayComponent } from './mhtk-id-card-display/mhtk-id-card-display';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [
        MhtkIdCardDisplayComponent
    ],
	imports: [
        IonicModule.forRoot(MhtkIdCardDisplayComponent)
    ],
	exports: [
        MhtkIdCardDisplayComponent
    ]
})
export class ComponentsModule {}
